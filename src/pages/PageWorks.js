import { Link } from "react-router-dom";
import { Button, Col, PageHeader, Row, Space } from 'antd';
import WorkDetails from '../components/works/WorkDetails';
import { useQuery, gql } from '@apollo/client';

const getAllWorks = gql`{
    allWorks {
      data {
        _id
        title
        synopsis
        authors {
          data {
            fullName
          }
        }
      }
    }
}`;

function Works() {
    const { loading, error, data } = useQuery(getAllWorks);
    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;
    console.log(data);
    const { allWorks } = data; 
    const workCards = allWorks.data.map(work => {
        const { _id, title, synopsis, authors: { data: authors } } = work;
        return <WorkDetails
            key={_id}
            title={title}
            synopsis={synopsis}
            authors={authors}
        />;
    });
    return (<div>
        <Row>
            <Col span={16}>
                <PageHeader
                    className="site-page-header"
                    onBack={() => null}
                    title="Works"
                    subTitle="This is a table containing all the works currently stored."
                />
            </Col>
            <Col
                span={8}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end'
                }}
            >
                <Button
                    type="primary"
                    style={{float: 'right', align: 'center'}}
                >
                    <Link to="/work/new">New Work</Link>
                </Button>
            </Col>
        </Row>
        <Space direction="vertical">
            { workCards }
        </Space>
    </div>);
};

export default Works;