import axios from 'axios';
import { useState, useEffect } from 'react';

import { Button, Card, Form, Input, PageHeader, Spin } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import WorkDetails from '../components/works/WorkDetails';
import WorkEditor from '../components/work/WorkEditor';

const OPEN_LIBRARY_SEARCH_API_ENDPOINT = 'https://openlibrary.org/search.json';
const OPEN_LIBRARY_URL = 'https://openlibrary.org';
const OPEN_LIBRARY_COVER_API_ENDPOINT = 'http://covers.openlibrary.org/b';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

function PageWorkEditor(props) {
    // TODO: (1) set spinner, (2) put suggestions in a table not in cards
    const [pending, setPending] = useState(false);
    const [items, setItems] = useState([]);
    const [work, setWork] = useState(null);
    const fetchWorkSuggestions = async values => {
        console.log(values);
        const { data: { docs = [] } } = await axios.get(OPEN_LIBRARY_SEARCH_API_ENDPOINT, {
            params: {
                q: values.suggestion
            }
        });
        setItems(docs);
    }
    const fetchWork = workKey => {
        return async function() {
            console.log(workKey);
            setPending(true);
            const {
                data: {
                    title,
                    description: {
                        value: synopsis
                    } = {},
                    authors: authorsData,
                    covers
                }
            } = await axios.get(`${OPEN_LIBRARY_URL}${workKey}.json`);
            // once you have fetched the work you should also fetch the cover URI and the author info (if available)
            // get all the authors info
            const authors = await Promise.all(authorsData.map(async({ author = {} }) => {
                const {
                    data: {
                        name,
                        birth_date: birthDate,
                        death_date: deathDate,
                        bio: {
                            value: bio
                        } = {}
                    }
                } = await axios.get(`${OPEN_LIBRARY_URL}${author.key}.json`);
                return {
                    name,
                    birthDate,
                    deathDate,
                    bio
                };
            }));
            setPending(false);
            setWork({
                title,
                synopsis,
                authors,
                coverLink: covers.length > 0 ? `${OPEN_LIBRARY_COVER_API_ENDPOINT}/id/${covers[0]}-M.jpg` : null
            });
            setItems([]); 
        }
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const suggestionCards = items.map(item => {
        const { key, title, author_name = [] } = item;
        return <Card
            key={key}
            title={title}
        >
            <p>{author_name.join()}</p>
            <Button type="primary" onClick={fetchWork(key)} >
                Select
            </Button>
        </Card>;
    })

    return <div>
        <PageHeader
            className="site-page-header"
            onBack={() => null}
            title="Work Editor"
            subTitle="On this page you can either create a new work or edit an existing one."
        />
        <div>
            <Form
                {...layout}
                onFinish={fetchWorkSuggestions}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Work Suggestions"
                    name="suggestion"
                >
                    <Input
                        size="large"
                        placeholder="search Works on the OpenLibrary API"
                    />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                </Form.Item>
            </Form>
            <div>
                {pending ? <Spin size="large" tip="Loading..." /> : suggestionCards}
            </div>
            <div>
                {work ? <WorkDetails {...work} /> : null}
            </div>
        </div>
    </div>
}

export default PageWorkEditor;