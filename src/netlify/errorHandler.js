/**
 * @module errorHandler;
 * @description Utility object for controller responses (wraps Node/Express response object).
 *              Inspired by a similar controller for Sails.js
 * @see {@link https://github.com/grokible/sails-tutorial-1/blob/master/TUTORIALS/TUTORIAL02.md}
 * @see {@link http://sailsjs.org/#/documentation/reference/res|Sails Response('res') Reference}
 */
 const env = process.env.NODE_ENV || 'development';
 const BAD_REQUEST_STATUS_CODE = 400;
 const SERVER_ERROR_STATUS_CODE = 500;
 
 class ErrorHandler {
 
     constructor(response) {
         this.response = response;
     }
 
     /**
      * @method
      * @name handle
      * @description this will handle a generic error for you depending on the error message
      */
     handle(error, options) {
         // Options properties {code, message} override error's
         // After options, underbar takes precedence (_code or _message) override error's
 
         options = options || {};
         let code = options.code || error._code || error.code || 'general.unknown';
 
         // Attempt to pull out any conceivable error message that we can use
 
         let message = options.message || error._message || error.message || error.description ||
             error.title || error.summary || error.reason || 'No message, unknown error.';
 
         let rc = {
             code: code,
             message: message
         };
 
         // console.log(error);
 
         // If we are in a Node dev environment, supply all the error details
         if (env === 'development') {
             rc._internal = error;
         }
 
         let statusCode = this.getStatusCodeForError(error);
 
         return {
             statusCode,
             handledError: rc
         };
     }
 
     /**
      * @method
      * @name badRequest
      
     badRequest(options) {
 
         options = options || {};
 
         let code = options.code || 'Bad Request';
 
         let message = options.message || 'Not authorized to this REST endpoint';
 
         return this.response.status(BAD_REQUEST_STATUS_CODE).json({
             code: code,
             message: message
         });
 
     } */
 
 
     /**
      * @method
      * @name unauthorized
     
     unauthorized(options) {
 
         options = options || {};
 
         let code = options.code || 'Unauthorized';
 
         let message = options.message || 'Not authendicated';
 
         return this.response.status(401).json({
             code: code,
             message: message
         });
 
     }  */
 
     /**
      * @method
      * @name forbidden
      
     forbidden(options) {
 
         options = options || {};
 
         let code = options.code || 'Forbidden';
 
         let message = options.message || 'Not authorized to this REST endpoint';
 
         return this.response.status(403).json({
             code: code,
             message: message
         });
 
     }*/
 
     /**
      * @method
      * @name getStatusCodeForError
      * @param{Error} error
      */
     getStatusCodeForError(error) {
 
         let errName = error.name || error.code;
 
         if (errName === 'ValidationError') {
             return BAD_REQUEST_STATUS_CODE;
         }
 
         if (errName === 'InvalidFormatError') {
             return BAD_REQUEST_STATUS_CODE;
         }
 
         if (errName === 'JsonWebTokenError') {
             return BAD_REQUEST_STATUS_CODE;
         }
         return SERVER_ERROR_STATUS_CODE;
 
     }
 
 }
 
 /**
  * @function
  * @name errorHandler
  * @param {Response} response
  */
 function errorHandler(response) {
     return new ErrorHandler(response);
 }
 
 module.exports = exports = errorHandler;
 