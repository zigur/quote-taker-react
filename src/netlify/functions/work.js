const errorHandler = require('../../app/utils/errorHandler');
const faunadb = require('faunadb'), q = faunadb.query;

const serverClient = new faunadb.Client({ secret: 'YOUR_FAUNADB_SERVER_SECRET' });

exports.handler = async event => {
    try {
        switch(event.httpMethod) {
            case 'POST': {
                const reqPayload = JSON.parse(event.body);
                const ret = await serverClient.query(
                    // first check if author(s) exists
                    // if yes do nothing (or update) else create
                    // then create work
                    q.Create("Work")
                );
                return {
                    statusCode: 200
                };
            }
            case 'GET': {
                return {
                    statusCode: 200
                };
            }
            case 'PUT':
                // TODO implement update
                return {
                    statusCode: 200
                };
            case 'DELETE':
                // TODO implement (soft?) delete
                return {
                    statusCode: 204
                };
            default:
                return {
                    statusCode: 400,
                    body: 'Unsupported HTTP verb. Supports POST, GET, PUT, DELETE.'
                }
        }
    } catch(err) {
        const { statusCode, handledError } = errorHandler.handle(err);
        return {
            statusCode,
            body: JSON.stringify(handledError)
        };
    }
};