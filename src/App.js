import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import {
    Layout
} from 'antd';
import PageQuotes from './pages/PageQuotes';
import PageWorks from './pages/PageWorks';
import PageWorkEditor from './pages/PageWorkEditor';
import NavBar from './components/layout/NavBar';
import SideBar from "./components/layout/SideBar";

import './App.css';
import AppMenu from "./components/layout/AppMenu";
import {
    ApolloProvider,
    ApolloClient,
    InMemoryCache,
    createHttpLink
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import {
    REACT_PUBLIC_FAUNA_SECRET
} from './secrets';

const FAUNA_GRAPHQL_ENDPOINT = 'https://graphql.fauna.com/graphql';

const httpLink = createHttpLink({
    uri: FAUNA_GRAPHQL_ENDPOINT,
});

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: REACT_PUBLIC_FAUNA_SECRET ? `Bearer ${REACT_PUBLIC_FAUNA_SECRET}` : "",
      }
    }
});

const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
});

class App extends React.Component {

    render() {
        const menu = <AppMenu />;
        return (
            <div className="App">
                <ApolloProvider client={client} >
                    <Router>
                        <NavBar menu={menu} />
                        <Layout>
                            <SideBar menu={menu} />
                            <Layout.Content
                                className="App-content"
                            >
                                <Switch>
                                    <Route path="/quotes">
                                        <PageQuotes />
                                    </Route>
                                    <Route path="/works">
                                        <PageWorks />
                                    </Route>
                                    <Route path="/work/new">
                                        <PageWorkEditor />
                                    </Route>
                                    <Route path="/work/edit/:workId">
                                        <PageWorkEditor />
                                    </Route>
                                    <Route path="/">
                                        <PageQuotes />
                                    </Route>
                                </Switch>                       
                            </Layout.Content>
                        </Layout>
                    </Router>
                </ApolloProvider>
            </div>
        );
    }
}
  
export default App;
