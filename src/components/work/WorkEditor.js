import Form from "antd/lib/form/Form";

function WorkEditor({ title, synopsis, authors = [], coverLink }) {
    return <Form
        title={`${title} - ${authors.map(author => author.fullName || author.name).join()}`}
        cover={
            coverLink ?
            <img
              alt="example"
              src={coverLink}
              className="cover"
            /> : null
        }
        bordered={true}
    >
        <div>
            {synopsis || 'No synopsis yet.'}
        </div>
    </Form>;
}

export default WorkEditor;