import {
    Link
} from "react-router-dom";
import { Menu } from 'antd';
import React from "react";
import {
    HomeOutlined,
    FileTextOutlined,
    BookOutlined,
  } from '@ant-design/icons';

const AppMenu = () => {
    return (<Menu
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        mode="inline"
        theme="dark"
        className="App-menu"
    >
        <Menu.Item key="1" icon={<HomeOutlined />}>
            <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<FileTextOutlined />}>
            <Link to="/quotes">Quotes</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<BookOutlined />}>
            <Link to="/works">Works</Link>
        </Menu.Item>
    </Menu>);
};

export default AppMenu;