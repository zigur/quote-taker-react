import { Card } from 'antd';
import './WorkDetails.css';

function WorkDetails({ title, synopsis, authors = [], coverLink }) {
    return <Card
        title={`${title} - ${authors.map(author => author.fullName || author.name).join()}`}
        cover={
            coverLink ?
            <img
              alt="example"
              src={coverLink}
              className="cover"
            /> : null
        }
        bordered={true}
    >
        <div>
            {synopsis || 'No synopsis yet.'}
        </div>
    </Card>;
}

export default WorkDetails;